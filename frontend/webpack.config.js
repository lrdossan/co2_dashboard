// webpack.config.js
module.exports = {
    // ... other webpack configuration options
  
    resolve: {
      fallback: {
        fs: false, // or require.resolve('path to a polyfill or alternative')
        util: require.resolve('util/'),
        path: require.resolve('path-browserify'),
      },
    },
  };