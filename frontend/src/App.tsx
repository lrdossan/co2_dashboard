import { useState, useEffect }  from 'react';
import './App.css';
import SignIn from './components/SignIn/SignIn';

function App() {

  return (
    <div className="App">
      <header>
        <SignIn />
      </header>
    </div>
  );
}

export default App;
