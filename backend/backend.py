import os
from flask import Flask, jsonify, session, request
from flask_cors import CORS
from grafana_script import GrafanaDriver

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})
app.secret_key = 'your_secret_key'  # Change this to a secure secret key

grafana_driver = GrafanaDriver()

# @app.route('/setup')
# def setup_driver():
#     grafana_driver.setup_driver()
#     return 'Driver setup successful'

@app.route('/login', methods=['POST'])
def login_driver():
    if grafana_driver.driver:
        data = request.get_json()

        if 'email' in data and 'password' in data:
            email = data['email']
            password = data['password']
            result = grafana_driver.login_with_credentials(email, password)
            return jsonify({'result': result})
        else:
            return jsonify({'error': 'Invalid request'})
    else:
        return 'Driver not set up'

@app.route('/quit')
def quit_driver():
    grafana_driver.quit_driver()
    return 'Driver quit successfully'

if __name__ == '__main__':
    app.run(debug=False, port=8080)
