import pandas as pd

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


import time
import os
import logging
from datetime import datetime
import re

import chromedriver_autoinstaller
chromedriver_autoinstaller.install()

# Configure logging
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


# Function to perform the login with credentials
def login_screen(driver, email, password):
    email_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.NAME, "user"))
    )
    email_field.send_keys(email)

    password_field = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.NAME, "password"))
    )
    password_field.send_keys(password)

    return


# # Funtion to handle time granularity
# def set_time_granularity(dashboard_title, dashboard_url, driver, granularity):
#     try:
#         driver.get(dashboard_url)
#         time.sleep(1)

#         original_query = WebDriverWait(driver, 10).until(
#             EC.visibility_of_element_located(
#                 (By.CSS_SELECTOR, 'textarea.css-1j4mfqt'))
#         )

#         # Get and modify the content
#         current_content = original_query.get_attribute('value')
#         modified_query = re.sub(
#             r'time\(\d+[a-zA-Z]\)', f'time({granularity})', current_content)

#         # Set the modified content back to the textarea
#         driver.execute_script("arguments[0].value = '';", original_query)
#         original_query.send_keys(modified_query)

#         # Click the Save dashboard button
#         save_dashboard_button = WebDriverWait(driver, 10).until(
#             EC.element_to_be_clickable(
#                 (By.CSS_SELECTOR, 'button.css-1eapwyv-button'))
#         )
#         save_dashboard_button.click()

#         # Click the Save button in the modal
#         save_modal_button = WebDriverWait(driver, 10).until(
#             EC.element_to_be_clickable(
#                 (By.CSS_SELECTOR, 'button.css-1x0il5t-button[aria-label="Dashboard settings Save Dashboard Modal Save button"]'))
#         )
#         save_modal_button.click()

#         logger.info(
#             f"New query successfully updated for dashboard '{dashboard_title}'.")

#     except Exception as e:
#         logger.error(f"Error during setting granularity: {e}. Exiting")
#         exit()


# # Function to handle downloaded CSV
# def handle_downloaded_file():
#     downloads_folder = os.path.expanduser('~') + '/Downloads/'
#     latest_file = max(
#         [downloads_folder + f for f in os.listdir(downloads_folder)], key=os.path.getctime)

#     # Read the downloaded CSV file into a DataFrame
#     csv_data = pd.read_csv(latest_file)

#     # Delete the downloaded file
#     os.remove(latest_file)

#     return csv_data


# # Function to download CSV data from Grafana panel and return as DataFrame
# def download_panel_data(driver, panel_xpath, formatted_data=False):
#     try:
#         if not formatted_data:
#             # Find the "Data options" element and click it
#             data_options_button = WebDriverWait(driver, 10).until(
#                 EC.element_to_be_clickable(
#                     (By.XPATH, '//div[@class="css-1prl574" and text()="Data options"]'))
#             )
#             data_options_button.click()

#             # Set "formatted-data-toggle" to false before downloading
#             formatted_data_toggle = WebDriverWait(driver, 10).until(
#                 EC.element_to_be_clickable(
#                     (By.XPATH, '//label[@for="formatted-data-toggle"]'))
#             )
#             formatted_data_toggle.click()

#         panel_download_button = WebDriverWait(driver, 10).until(
#             EC.element_to_be_clickable((By.XPATH, panel_xpath))
#         )
#         panel_download_button.click()

#         time.sleep(1)  # Add a delay to allow the download to start

#         return handle_downloaded_file()

#     except Exception as e:
#         logger.error(f"Error during panel data download: {e}")
#         return pd.DataFrame()


# def prompt_for_dashboard():
#     print("Choose a dashboard:")
#     print("1. sn269-luiss-office")
#     print("2. sn266-andres-office")
#     print("3. sn265")
#     print("4. sn267")
#     print("5. sn268")
#     choice = input("Enter the number corresponding to the desired dashboard: ")
#     return {
#         "1": "296VqOsnz/sn269-luiss-office",
#         "2": "84Xnqdsnz/sn266-andres-office",
#         "3": "dU8ICdsnk/sn265",
#         "4": "WVS43Oynz/sn267",
#         "5": "fmb43dy7z/sn268",
#     }.get(choice, None)


# def dashboard_urls(selected_dashboard, mode, time_granularity=None, from_ms=None, to_ms=None):
#     if time_granularity:
#         return {
#             'Temperature': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=18',
#             'Humidity': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=17',
#             'Aerosols': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=21',
#             'CO2': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=4',
#             'Chemicals (TVOC)': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=10',
#             'AQI': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=24',
#         }
#     elif from_ms and to_ms:
#         return {
#             'Temperature': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=18&from={from_ms}&to={to_ms}',
#             'Humidity': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=17&from={from_ms}&to={to_ms}',
#             'Aerosols': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=21&from={from_ms}&to={to_ms}',
#             'CO2': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=4&from={from_ms}&to={to_ms}',
#             'Chemicals (TVOC)': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=10&from={from_ms}&to={to_ms}',
#             'AQI': f'https://grafana.arve.swiss/d/{selected_dashboard}?orgId=1&{mode}=24&from={from_ms}&to={to_ms}',
#         }


# def prompt_for_time_granularity():
#     while True:
#         try:
#             choice = input(
#                 "Enter the time granularity (ex: '5m', '30m', etc.): ")
#             # Check if the input follows the format of integer + 'm'
#             if not choice[:-1].isdigit() or choice[-1] != 'm':
#                 raise ValueError(
#                     "Invalid time granularity format. Please use the format 'Xm', where X is an integer.")
#             return choice
#         except ValueError as e:
#             print(f"{e}. Exiting.")
#             exit()


# def prompt_for_time_frame():
#     date_format = "%Y-%m-%d %H:%M:%S"

#     while True:
#         try:
#             from_time = input(
#                 f"Enter the 'from' time in the format 'YYYY-MM-DD hh:mm:ss': ")

#             # Validate the input format
#             from_time_formatted = datetime.strptime(from_time, date_format)

#             to_time = input(
#                 f"Enter the 'to' time in the format 'YYYY-MM-DD hh:mm:ss': ")
#             # Validate the input format
#             to_time_formatted = datetime.strptime(to_time, date_format)

#             return from_time_formatted, to_time_formatted
#         except ValueError:
#             print(
#                 "Invalid date format. Please enter the date in the format 'YYYY-MM-DD hh:mm:ss'.")


# # Function to convert a datetime string to milliseconds since the Unix epoch
# def convert_to_milliseconds(time_frame: datetime):
#     return int(time_frame.timestamp()) * 1000


# # Function to validate if "to" is after "from" and no later than now
# def validate_time_range(from_datetime, to_datetime):
#     now_datetime = datetime.now()

#     if from_datetime >= to_datetime:
#         raise ValueError("The 'to' time must be after the 'from' time.")
#     elif to_datetime > now_datetime:
#         raise ValueError(
#             "The 'to' time cannot be later than the current time.")


# # Function to check if the dashboard has "No data"
# def has_no_data(driver):
#     try:
#         # Look for the "No data" element
#         no_data_element = driver.find_element(
#             By.XPATH, '//div[contains(text(), "No data")]')
#         return no_data_element.is_displayed()
#     except NoSuchElementException:
#         return False


# def setup_driver() -> webdriver.Chrome():
#     # Set up Chrome driver
#     chrome_options = webdriver.ChromeOptions()
#     chrome_options.add_argument("--headless=new")

#     # Assuming you have already created a WebDriver instance
#     driver = webdriver.Chrome()

#     # Navigate to the Grafana URL
#     grafana_url = "https://grafana.arve.swiss"
#     driver.get(grafana_url)

#     # Check if the page has a certificate error
#     logger.info("Handling certificate...")
#     try:
#         certificate_error_element = WebDriverWait(driver, 10).until(
#             EC.presence_of_element_located((By.ID, "details-button"))
#         )
#         certificate_error_element.click()
#         time.sleep(1)

#         # Wait for the "Proceed to grafana.arve.swiss (unsafe)" button
#         proceed_button = WebDriverWait(driver, 10).until(
#             EC.presence_of_element_located((By.ID, "proceed-link"))
#         )
#         proceed_button.click()

#     except Exception as e:
#         logger.error(f"Error during certificate handling: {e}")
#         driver.quit()

#     return driver


# def login_with_credentials(driver, email, password):
#     # Locate and fill in email and password fields
#     logger.info("Logging in to Grafana...")
#     try:
#         login_screen(driver=driver, email=email, password=password)

#     except Exception as e:
#         logger.error(f"Error during filling in credentials: {e}")
#         driver.quit()

#     # Locate and click the login button
#     logger.info("Loggin in...")
#     try:
#         login_button = WebDriverWait(driver, 10).until(
#             EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[type="submit"]'))
#         )
#         login_button.click()

#         # Check for an element indicating invalid credentials
#         invalid_credentials_element = WebDriverWait(driver, 1).until(
#             EC.presence_of_element_located((By.XPATH, '//*[contains(text(), "Invalid username or password")]'))
#         )

#         # If the invalid credentials element is found, handle it
#         if invalid_credentials_element:
#             logger.error("Invalid username or password. Exiting.")
#             exit()

#     except TimeoutException:
#         # Continue with the rest of the script if login is successful
#         pass

#     except NoSuchElementException as e:
#         logger.error(f"Error during clicking the login button: {e}")
#         driver.quit()

#     logger.info("Login successful!")

# # Prompt the user for the desired dashboard
# selected_dashboard = prompt_for_dashboard()
# if selected_dashboard is None:
#     logger.error("Invalid choice. Exiting.")
#     exit()

# logger.info(f'Selected dashboard: {selected_dashboard}')

# # Prompt the user for the time interval (granularity)
# time_granularity = prompt_for_time_granularity()
# logger.info(f'Granularity: {time_granularity}')

# dashboards = dashboard_urls(selected_dashboard, 'editPanel', time_granularity)

# # Navigate to the desired dashboard URL to set granularity
# for dashboard_title, dashboard_url in tqdm(dashboards.items(), desc="Processing Dashboards"):
#     set_time_granularity(dashboard_title, dashboard_url, driver, time_granularity)

# logger.info('All panels updated.')

# # Prompt the user for the time frame
# from_time, to_time = prompt_for_time_frame()

# # Validate the time range
# try:
#     validate_time_range(from_time, to_time)
# except ValueError as e:
#     logger.error(f"Error: {e}")
#     driver.quit()

# # Convert the input time frames to milliseconds
# from_ms = convert_to_milliseconds(from_time)
# to_ms = convert_to_milliseconds(to_time)

# time.sleep(1)
# dashboards = dashboard_urls(selected_dashboard, mode='inspect', from_ms=from_ms, to_ms=to_ms)

# # Initialize an empty DataFrame to store the final data
# final_df = pd.DataFrame()

# # Navigate to the desired dashboard URL to download data
# for dashboard_title, dashboard_url in tqdm(dashboards.items(), desc="Processing Dashboards"):
#     driver.get(dashboard_url)
#     time.sleep(1)

#     # Check if the dashboard has "No data"
#     if has_no_data(driver):
#         logger.error(f"The dashboard '{dashboard_title}' has 'No data'. Exiting.")
#         exit()

#     # Download data from panels
#     try:
#         panel_download_button = '//button[@class="css-19lk5s2-button" and span[text()="Download CSV"]]'
#         df_panel = download_panel_data(driver, panel_download_button)

#         logger.info(f"Downloaded data for dashboard: {dashboard_title}")
#         # Print the first few rows of the downloaded data for debugging
#         print(df_panel.head())
#         print(len(df_panel.index))

#         # Merge or concatenate the DataFrames based on the common column (time)
#         if not df_panel.empty:
#             if not final_df.empty:
#                 final_df = pd.merge(final_df, df_panel, on='Time', how='outer')
#             else:
#                 final_df = df_panel

#     except Exception as e:
#         logger.error(
#             f"Error during panel data download with formatting for dashboard {dashboard_title}: {e}")
#         driver.quit()


# # Download one of the panels without the formatted data
# driver.get(dashboard_url)
# time.sleep(1)

# # Set first column (Time) correctly
# logger.info("Set formatted Time...")
# try:
#     # Download data from panels (Example: assuming download buttons have specific class)
#     panel_download_button = '//button[@class="css-19lk5s2-button" and span[text()="Download CSV"]]'

#     # Download data from the panel
#     df_panel = download_panel_data(
#         driver, panel_download_button, formatted_data=True)

#     # Replace the first column in the final DataFrame with the first column from the data without formatting
#     if not df_panel.empty and not final_df.empty:
#         final_df.iloc[:, 0] = df_panel.iloc[:, 0]

# except Exception as e:
#     logger.error(
#         f"Error during panel data download without formatting for dashboard {dashboard_title}: {e}")
#     driver.quit()

# # Create a DataFrame with the time frame and sensor selected
# metadata_df = pd.DataFrame({
#     'Time Frame': [f'{from_time} to {to_time}'],
#     'Selected Sensor': [selected_dashboard]
# })

# # Concatenate the metadata DataFrame with the final data DataFrame
# final_df = pd.concat([final_df, metadata_df], axis=1)

# final_excel_path = f"grafana_data_{selected_dashboard.split('/')[-1]}.xlsx"
# if os.path.exists(final_excel_path):
#     os.remove(final_excel_path)
# final_df.to_excel(final_excel_path, index=False)
# logger.info(f"Final data saved to {final_excel_path}")

# # Print a message indicating that the script has completed
# print(final_df)
# logger.info("Script execution completed successfully.")

# driver.quit()

class GrafanaDriver:
    def __init__(self):
        self.driver = None
        self.setup_driver()

    def setup_driver(self):
        # Set up Chrome driver only if it hasn't been initialized yet
        if not self.driver:
            # Set up Chrome options (add additional options if needed)
            chrome_options = webdriver.ChromeOptions()
            # chrome_options.add_argument("--headless=new")

            # Create a WebDriver instance
            self.driver = webdriver.Chrome(options=chrome_options)

            # Navigate to the Grafana URL
            grafana_url = "https://grafana.arve.swiss"
            self.driver.get(grafana_url)

            # Handle certificate error
            try:
                certificate_error_element = WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.ID, "details-button"))
                )
                certificate_error_element.click()
                WebDriverWait(self.driver, 10).until(
                    EC.presence_of_element_located((By.ID, "proceed-link"))
                ).click()
            except Exception as e:
                print(f"Error during certificate handling: {e}")
                self.driver.quit()

    def login_with_credentials(self, email, password):
        # Locate and fill in email and password fields
        logger.info("Logging in to Grafana...")
        try:
            login_screen(driver=self.driver, email=email, password=password)

        except Exception as e:
            logger.error(f"Error during filling in credentials: {e}")
            self.driver.quit()

        # Locate and click the login button
        logger.info("Loggin in...")
        try:
            login_button = WebDriverWait(self.driver, 10).until(
                EC.element_to_be_clickable(
                    (By.CSS_SELECTOR, 'button[type="submit"]'))
            )
            login_button.click()

            # Check for an element indicating invalid credentials
            invalid_credentials_element = WebDriverWait(self.driver, 1).until(
                EC.presence_of_element_located(
                    (By.XPATH, '//*[contains(text(), "Invalid username or password")]'))
            )

            # If the invalid credentials element is found, handle it
            if invalid_credentials_element:
                logger.error("Invalid username or password. Exiting.")
                exit()

        except TimeoutException:
            # Continue with the rest of the script if login is successful
            pass

        except NoSuchElementException as e:
            logger.error(f"Error during clicking the login button: {e}")
            self.driver.quit()

        logger.info("Login successful!")

    def quit_driver(self):
        if self.driver:
            self.driver.quit()
